fn find_common_char(a: &str, b: &str) -> Option<char> {
    a.chars()
        .filter(|c| b.contains(*c)).next()
}

fn calculate_priority(c : char) -> i32 {
    match c {
        'a'..='z' => c as i32 - 'a' as i32 + 1,
        'A'..='Z' => c as i32 - 'A' as i32 + 27,
        _ => panic!("Unexpected char"),
    }
}

fn main() {
    let input = include_str!("../inputs/input03.txt");

    let res : i32 = input
        .lines()
        .map(|s| s.split_at(s.len()/2))
        .map(|(a,b)|find_common_char(a,b))
        .map(|c| calculate_priority(c.unwrap()))
        .sum();
    println!("{:?}",res);
}