fn main() {
    let input = include_str!("../inputs/input04.txt");

    let res = input
        .lines()
        .map(
            |l| l.
                split_terminator(',')
                .map(|range| range
                    .split_once('-')
                    .map(|(min, max)|
                        min.parse::<u32>().unwrap()..=max.parse::<u32>().unwrap())
                    .unwrap()
                ))
        .map(|mut pair|
            (pair.next().unwrap(),
             pair.next().unwrap())
        )
        .fold(0, |acc, (r1, r2)| {
            if r1.contains(r2.start()) && r1.contains(r2.end())
                || r2.contains(r1.start()) && r2.contains(r1.end()) {
                return acc + 1;
            }
            acc
        });

    println!("{:?}", res);
}