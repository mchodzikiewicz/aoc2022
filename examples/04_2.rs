use std::cmp::{max, min};
use std::ops::RangeInclusive;

fn common_part<'a, T>(r1: &'a RangeInclusive<T>, r2: &'a RangeInclusive<T>) -> Option<RangeInclusive<T>>
    where T: Ord + Copy,
{
    let common_part = max(*r1.start(), *r2.start())..=min(*r1.end(), *r2.end());
    match common_part.is_empty() {
        true => None,
        false => Some(common_part),
    }
}

fn main() {
    let input = include_str!("../inputs/input04.txt");

    let res = input
        .lines()
        .map(
            |l| l.
                split_terminator(',')
                .map(|range| range
                    .split_once('-')
                    .map(|(min, max)|
                        min.parse::<u32>().unwrap()..=max.parse::<u32>().unwrap())
                    .unwrap()
                ))
        .map(|mut pair|
            (pair.next().unwrap(),
             pair.next().unwrap())
        )
        .fold(0, |acc, (r1, r2)| {
            if let Some(_) = common_part(&r1, &r2) {
                return acc + 1;
            }
            acc
        });

    println!("{:?}", res);
}