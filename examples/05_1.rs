use itertools::Itertools;
use regex::Regex;

fn main() {
    let input = include_str!("../inputs/input05.txt");

    let (state, instr) = input
        .split_once("\n\n").unwrap();

    let mut state = state.lines().rev();
    let rows = state.next().unwrap();
    let rows = rows
        .split_ascii_whitespace()
        .rev()
        .next().unwrap()
        .parse::<u32>().unwrap();
    let mut cargo = Vec::new();
    for _ in 0..rows {
        cargo.push(Vec::<char>::new());
    }

    state
        .for_each(|l| l
            .chars()
            .chunks(4)
            .into_iter()
            .zip(&mut cargo)
            .for_each(|(mut c, cargo)| {
                if c.contains(&'[') {
                    cargo.push(c.nth(0).unwrap())
                }
            })
        );

    let re = Regex::new(r"\D+(\d+)\D+(\d+)\D+(\d+)").unwrap();

    re.captures_iter(instr)
        .map(|c| (c[1].parse::<usize>().unwrap(),
                  c[2].parse::<usize>().unwrap(),
                  c[3].parse::<usize>().unwrap()))
        .for_each(|(count, from, to)| {
            for _ in 0..count {
                let cr = cargo[from - 1].pop().unwrap();
                cargo[to - 1].push(cr);
            }
        });

    let mut res = String::new();
    for mut row in cargo {
        res.push(row.pop().unwrap())
    }
    println!("{:?}", res);
}