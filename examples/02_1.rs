#[derive(Debug)]
enum RpcSign {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug)]
enum GameResult {
    Win,
    Loose,
    Draw,
}

impl RpcSign {
    fn from_opponent_notation(input: &str) -> Self {
        match input {
            "A" => Self::Rock,
            "B" => Self::Paper,
            "C" => Self::Scissors,
            _ => panic!("Wrong notation!"),
        }
    }

    fn from_your_notation(input: &str) -> Self {
        match input {
            "X" => Self::Rock,
            "Y" => Self::Paper,
            "Z" => Self::Scissors,
            _ => panic!("Wrong notation!"),
        }
    }
}

fn game_result(opponent: &RpcSign, your: &RpcSign) -> GameResult {
    match your {
        RpcSign::Rock => match opponent {
            RpcSign::Rock => GameResult::Draw,
            RpcSign::Paper => GameResult::Loose,
            RpcSign::Scissors => GameResult::Win,
        }
        RpcSign::Paper => match opponent {
            RpcSign::Rock => GameResult::Win,
            RpcSign::Paper => GameResult::Draw,
            RpcSign::Scissors => GameResult::Loose,
        }
        RpcSign::Scissors => match opponent {
            RpcSign::Rock => GameResult::Loose,
            RpcSign::Paper => GameResult::Win,
            RpcSign::Scissors => GameResult::Draw,
        }
    }
}

fn score(res: &GameResult, your_sign: &RpcSign) -> u32 {
    let match_res = match res {
        GameResult::Win => 6,
        GameResult::Loose => 0,
        GameResult::Draw => 3,
    };
    let sign_points = match your_sign {
        RpcSign::Rock => 1,
        RpcSign::Paper => 2,
        RpcSign::Scissors => 3,
    };
    match_res + sign_points
}

fn main() {
    let input = include_str!("../inputs/input02.txt");

    let res: u32 = input
        .lines()
        .map(|l| l.split_ascii_whitespace())
        .map(|mut l| (
            RpcSign::from_opponent_notation(l.next().expect("No opponent input")),
            RpcSign::from_your_notation(l.next().expect("No your input")))
        )
        .map(|(opponent, yours)| score(&game_result(&opponent, &yours), &yours))
        .sum();

    println!("{:?}", res);
}