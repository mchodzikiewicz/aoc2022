use itertools::*;

fn find_common_char(a: &str, b: &str, c: &str) -> Option<char> {
    a.chars()
        .filter(|ch| b.contains(*ch))
        .filter(|ch| c.contains(*ch))
        .next()
}

fn calculate_priority(c : char) -> i32 {
    match c {
        'a'..='z' => c as i32 - 'a' as i32 + 1,
        'A'..='Z' => c as i32 - 'A' as i32 + 27,
        _ => panic!("Unexpected char"),
    }
}

fn main() {
    let input = include_str!("../inputs/input03.txt");

    let res : i32 = input
        .lines()
        .chunks(3)
        .into_iter()
        .map(|mut chunk| find_common_char(chunk.next().unwrap(),chunk.next().unwrap(),chunk.next().unwrap()))
        .map(|c| calculate_priority(c.unwrap()))
        .sum();
    println!("{:?}",res);
}