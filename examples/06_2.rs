use std::collections::VecDeque;
use itertools::Itertools;

const BUF_LEN: usize = 14;

fn main() {
    let input = include_str!("../inputs/input06.txt");
    let res = input
        .as_bytes()
        .windows(BUF_LEN)
        .position(|w|
            (0..w.len())
                .all(|i|
                    !w[i + 1..]
                        .contains(&w[i])
                )
        )
        .map(|i| i + n)
        .unwrap();
    println!("{}", res);
}