fn main() {
    let input = include_str!("../inputs/input1.txt");

    let input: u32 = input
        .split_terminator("\n\n")
        .map(|s| s.split_ascii_whitespace()
            .map(|s| s.parse::<u32>().expect("Failed to parse a number"))
            .sum()
        )
        .max().expect("No maximum element");

    println!("{:?}",input);
}