use std::collections::VecDeque;
use itertools::Itertools;
use factorial::Factorial;

const BUF_LEN: usize = 4;

fn main() {
    let input = include_str!("../inputs/input06.txt");
    let mut buf = VecDeque::new();
    let mut pos = 0;
    for c in input.chars() {
        buf.push_back(c);
        if buf.len() < BUF_LEN + 1 {
            continue;
        }
        buf.pop_front();
        //ok, this solution is funny and ridicoulusly complex computationwise :D
        let uniques = buf.iter().permutations(buf.len()).unique().count();
        if uniques == BUF_LEN.factorial() {
            break;
        }
        println!("buf: {buf:?}, uniques: {uniques}");
        pos += 1;
    }
    println!("{:?}", pos + 5);
}