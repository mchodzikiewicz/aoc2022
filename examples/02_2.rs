#[derive(Debug)]
enum RpcSign {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug)]
enum GameResult {
    Win,
    Loose,
    Draw,
}

fn from_desired_res(input: &str) -> GameResult {
    match input {
        "X" => GameResult::Loose,
        "Y" => GameResult::Draw,
        "Z" => GameResult::Win,
        _ => panic!("Wrong notation!"),
    }
}

impl RpcSign {
    fn shape_to_achieve_result(opponent: &RpcSign, desired_res: &GameResult) -> RpcSign {
        match desired_res {
            GameResult::Win => match opponent {
                RpcSign::Rock => RpcSign::Paper,
                RpcSign::Paper => RpcSign::Scissors,
                RpcSign::Scissors => RpcSign::Rock,
            }
            GameResult::Draw => match opponent {
                RpcSign::Rock => RpcSign::Rock,
                RpcSign::Paper => RpcSign::Paper,
                RpcSign::Scissors => RpcSign::Scissors,
            }
            GameResult::Loose => match opponent {
                RpcSign::Rock => RpcSign::Scissors,
                RpcSign::Paper => RpcSign::Rock,
                RpcSign::Scissors => RpcSign::Paper,
            }
        }
    }

    fn score(res: &GameResult, your_sign: &RpcSign) -> u32 {
        let match_res = match res {
            GameResult::Win => 6,
            GameResult::Loose => 0,
            GameResult::Draw => 3,
        };
        let sign_points = match your_sign {
            RpcSign::Rock => 1,
            RpcSign::Paper => 2,
            RpcSign::Scissors => 3,
        };
        match_res + sign_points
    }

    fn from_opponent_notation(input: &str) -> Self {
        match input {
            "A" => Self::Rock,
            "B" => Self::Paper,
            "C" => Self::Scissors,
            _ => panic!("Wrong notation!"),
        }
    }
}

fn main() {
    let input = include_str!("../inputs/input02.txt");

    let res: u32 = input
        .lines()
        .map(|l| l.split_ascii_whitespace())
        .map(|mut l| (
            RpcSign::from_opponent_notation(l.next().expect("No opponent input")),
            from_desired_res(l.next().expect("No desired res input")))
        )
        .map(|(opponent, desired_res)| RpcSign::score(&desired_res, &RpcSign::shape_to_achieve_result(&opponent, &desired_res)))
        .sum();

    println!("{:?}", res);
}